import React from 'react';
import { Text , View , StyleSheet , FlatList , TouchableOpacity , ScrollView , TextInput} from 'react-native';
import { style } from '../style';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { url } from '../url';
import io from 'socket.io-client';

class Message extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            data : [],
            unread : 0,
            message : '',
            connected : false,
        }
    }

    socket = io(url,{
                path : '/messages',
                query : {
                    token : this.props.token,
                    email : this.props.email,
                }
            });

    componentDidMount(){
        fetch(url+'/event/getmessages/'+this.props.id,{
            headers : {
                "Authorization" : "Bearer "+this.props.token
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState({data : res['result'].messages , unread : res['result'].unread});
            }
        })
        .catch(err=>{
            console.log(err);
        });

        this.socket.on('connect',()=>{
            this.socket.on('incoming message',(obj)=>{
                console.log(obj);
                this.setState(()=>{
                    let obj1 = {};
                    Object.assign(obj1,this.state);
                    obj1.data.push(obj);
                    return obj1;
                });
            });
        })
    }

    componentWillUnmount(){
        this.socket.disconnect();
    }

    send(){
        if(this.state.message.length == 0) return;
        let obj = {
            content : this.state.message,
            _id : this.props.id,
            email : this.props.email,
            name : this.props.name,
            time : new Date(),
        };
        this.socket.emit('new message',obj);
        this.setState(()=>{
            let obj1 = {};
            Object.assign(obj1,this.state);
            obj1.data.push(obj);
            obj1.message = '';
            return obj1;
        });
    }

    render(){
        return (
            <View style={style.container}> 
                <View style={style.header}><Text style={style.header_text}>{this.props.sport}</Text></View>
                <View style={styles.margin}>
                    <ScrollView style={styles.msgPanel}>
                        <FlatList 
                        data={this.state.data}
                        keyExtractor={(item)=>(new Date(item.time).toString())}
                        extraData={this.state}
                        style={{marginBottom : 10}}
                        renderItem = {({item})=>(
                            <View style = {item.email == this.props.email ? styles.user : styles.friend}>
                                <Text style={styles.name}>{item.name}</Text>
                                <Text style={styles.txt}>{item.content}</Text>
                            </View>
                        )}/>

                    </ScrollView>
                    <View style={styles.send}>
                        
                        <TextInput 
                            style={styles.input}
                            placeholder="enter your message here" 
                            onChangeText={(text)=>{this.setState({message : text})}} 
                            value={this.state.message}
                            multiline = {true}
                        />
                        
                        <TouchableOpacity
                        style={styles.btn} 
                        onPress={()=>{this.send()}}>
                            <Icon name = {'send'} color={'#0098fd'} size = {25} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    send:{
        flexDirection : 'row',
        alignItems : 'center',
        height : 50,
        paddingHorizontal : 10,
        backgroundColor : '#DDE1E3' 
    },
    msgPanel : {
        height : 436,
        backgroundColor : "#CAD5DC",
        padding : 10, 
    },
    input : {
        width : 300,
        backgroundColor : '#fff',
        borderRadius : 10,
        marginRight : 10,
        paddingLeft : 5,
        height : 40,
    },
    btn :{
        width : 60,
    },
    friend:{
        maxWidth : 300 ,
        minWidth : 100, 
        alignSelf : 'flex-start',
        backgroundColor : '#E12765',
        padding : 10,
        borderRadius : 5,
        marginTop : 5,
        marginBottom : 5,
    },
    user : {
        maxWidth : 300,
        minWidth : 100,
        alignSelf : "flex-end",
        backgroundColor : '#0098fd',
        padding : 10,
        borderRadius : 5,
        marginTop : 5,
        marginBottom : 5,
    },
    txt : {
        color : '#fff',
    },
    name:{
        fontSize : 10,
        color : 'white'
    }
})

const mapStateToProps = state => ({
    id : state.chat.id,
    sport : state.chat.sport,
    token : state.user.token,
    email : state.user.email,
    name : state.user.name
})

export default connect(mapStateToProps)(Message);

