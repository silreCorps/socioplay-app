import React from 'react';
import { Text , View , FlatList , TouchableOpacity , StyleSheet , Image , ActivityIndicator , RefreshControl , ScrollView } from 'react-native';
import {style} from '../style';
import {url} from '../url';
import {connect} from 'react-redux';
import {setChat} from '../actions/notifier';

class Messages extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            data : [],
            loading : false,
            initial : false
        }
    }

    componentDidMount(){
        this.setState({initial: true})
        fetch(url+'/users/upcomingevents',{
            headers : {
                'Authorization' : 'Bearer '+this.props.token,
            }
        })
        .then(res => res.json())
        .then(res=>{
            this.setState({data : res['result'],initial : false});
        })
        .catch(err=>{
            console.log(err);
        })
    }

    refresh(){
        this.setState({loading : true})
        fetch(url+'/users/upcomingevents',{
            headers : {
                'Authorization' : 'Bearer '+this.props.token,
            }
        })
        .then(res => res.json())
        .then(res=>{
            this.setState({data : res['result'],loading : false});
        })
        .catch(err=>{
            console.log(err);
        })
    }

    render(){
        if(this.state.initial) return (
            <View style={{flex : 1 , justifyContent : "center" , alignItems : "center"}}>
                <ActivityIndicator size = {50} color = "#0098fd" /> 
            </View>
            );

        return (
            <View style={style.container}>
            <View style={style.header}><Text style={style.header_text}>My Matches</Text></View>
                <ScrollView 
                    refreshControl={
                        <RefreshControl 
                        refreshing={this.state.loading}
                        onRefresh={this.refresh.bind(this)}
                        />
                    }
                >
                    <FlatList 
                        keyExtractor={(item)=>item._id}
                        data={this.state.data}
                        renderItem={
                            ({item})=>(
                                <View>
                                    <View style={styles.chat}>
                                        <Image style = {styles.image} source={{uri : item.image}} />
                                        <View style= {styles.text}>
                                            <Text style={styles.sport}>{item.sport}</Text>
                                            <Text>{new Date(item.date_time).toDateString()}</Text>
                                        </View>
                                    </View>
                                    <View style = {styles.btngrp}>
                                        <TouchableOpacity style={styles.bt3} onPress={()=>{this.props.navigation.navigate('match',{id : item._id})}}>
                                            <Text style={styles.txt}>Info</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.bt3} onPress={()=>{this.props.dispatch(setChat({_id : item._id , sport : item.sport}));this.props.navigation.navigate('message')}}>
                                            <Text style={styles.txt}>Messages</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            )
                        }
                    style={styles.list}/>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    sport :{
        fontWeight : "500"
    },
    text : {
        paddingLeft : 20,
        justifyContent : 'center'
    },
    image : {
        width : 60,
        height : 60,
        borderRadius : 30
    },
    chat : {
        flexDirection : 'row',
        marginBottom : 10,
        marginTop : 10,
    },
    list : {
        margin : 20,
    },
    bt3:{
        borderColor : '#6BC6FF',
        borderWidth : 1,
        paddingHorizontal : 10,
        width : 140,
        justifyContent : "center",
        alignItems : 'center',
        borderRadius : 5,
        height : 30,
        marginHorizontal : 20
    },
    txt : {
        fontWeight : '500',
        fontSize : 16,
    },
    btngrp:{
        alignSelf : 'center',
        flexDirection : 'row',
        justifyContent : "space-between"
    }
});

mapStateToProp = state => ({
    token : state.user.token
});

export default connect(mapStateToProp)(Messages);