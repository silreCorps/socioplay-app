import React from 'react';
import { Text , View , StyleSheet , TextInput , TouchableOpacity } from 'react-native';
import {style} from '../style';
import AutoComplete from 'react-native-autocomplete-input';
import DatePicker from 'react-native-datepicker';
import Icon  from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import {url} from '../url';
import moment from 'moment';
import {connect} from 'react-redux';

class New_Match extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            query : '',
            sport : '',
            location : '',
            date : '',
            note : '',
            contact : '',
            loading : false,
            err : '',
            errStatus : false,
        }
    }

    create(){
        let o = this.state;
        let str1 = 'Please fill ',str2 = '';
        if(o.location && o.sport && o.date){
            this.setState({loading : true});
            fetch(url+'/event/createevent',{
                method : "POST",
                headers : {
                    "Authorization" : "bearer "+this.props.token,
                    "Content-type" : "application/json; charset=utf-8",
                },
                body : JSON.stringify({
                    sport : o.sport,
                    date_time : moment(o.date,"YYYY-MM-DD, h:mm a").toDate(),
                    address : o.location,
                    note : o.note,
                    contact : o.contact,
                    organizer : this.props.email,
                    participants : [],
                })
            })
            .then(res=>res.json())
            .then(res=>{
                if(res['status']==200){
                    this.setState({loading : false,err : ''});
                    this.props.navigation.navigate('show',{id : res['result']});
                }
                else{
                    this.setState({err : 'unable to reach server , please try again later' , loading : false})
                }
            })
            .catch(err=>{
                this.setState({err : 'unable to reach server , please try again later' , loading : false})
            })
        }
        else{
            if(!o.location) str2+= "Location field";
            if(!o.sport) str2+="sport field";
            if(!o.date) str2+="date and time field";
            this.setState({err : str1+str2 , errStatus : true});
        }
    }

    render(){
        let {query} = this.state;
        const data = query ? sports.filter(v => v.toLowerCase().indexOf(query.toLowerCase()) > -1).slice(0, 10) : [];
        return (
            <View style={style.container}>
                <View style={style.header}><Text style={style.header_text}>Create Match</Text></View>
                { this.state.err > 0 && 
                    <View>
                        <Text>{this.state.err}</Text>
                        <TouchableOpacity onPress={()=>{this.create()}}><Icon name="refresh" size={30}/></TouchableOpacity>
                    </View>
                }
                <View style = {styles.form}>
                    {this.state.errStatus && <Text style={styles.error}>{this.state.err}</Text>}
                    <View style={styles.flex}>
                        <View style={styles.icon}>
                            <Icon size={20} width={20} name="soccer-ball-o" color="#2e9be4" />
                        </View>
                        <AutoComplete
                            underlineColorAndroid = '#2e9be4'
                            placeholder = "Sport"
                            placeholderTextColor = '#A6A4A4'
                            containerStyle = {styles.i}
                            inputContainerStyle = {styles.sport}
                            defaultValue = {this.state.sport}
                            listStyle = {styles.box}
                            data = {data}
                            onChangeText={ text => this.setState({query : text})}
                            renderItem = { item => (
                                <TouchableOpacity onPress={()=>this.setState({query : '',sport : item})}><Text>{item}</Text></TouchableOpacity>
                            )}
                        />
                    </View>
                    <View style={styles.flex}>
                            <View style={styles.icon}>
                                <Icon1 size={20} width={20} name="place" color="#2e9be4" />
                            </View>
                        <TextInput 
                            placeholder="Location"
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            onChangeText={text=> this.setState({location : text})}
                            style={styles.input}
                        />
                    </View>
                    <View style={styles.flex}>
                            <View style={styles.icon}>
                                <Icon size={20} width={20} name="calendar" color="#2e9be4" />
                            </View>
                        {
                        //     below code should be uncommented and above icon component should be removed after ejecting 
                        //    <Icon size={20} width={20} name="calendar-alt" color="#2e9be4" />
                        }
                        <DatePicker
                            style={styles.input}
                            date={this.state.date}
                            mode="datetime"
                            placeholder="Select Date and Time"
                            format="YYYY-MM-DD, h:mm a"
                            minDate="2018-05-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon = {false}
                            onDateChange={(date) => {this.setState({date: date})}}
                        />
                    </View>
                    <View style={styles.flex}>
                        <View style={styles.icon}>
                            <Icon1 size={20} width={20} name="priority-high" color="#2e9be4" />
                        </View>
                        <TextInput 
                            placeholder="Note"
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            onChangeText={text=> this.setState({note : text})}
                            style={styles.input}
                            multiline={true}
                        />
                    </View>
                    <View style={styles.flex}>
                        <View style={styles.icon}>
                            <Icon size={25} width={25} name="mobile" color="#2e9be4" />
                            {
                                //     below code should be uncommented and above icon component should be removed after ejecting 
                                //    <Icon size={20} width={20} name="calendar-alt" color="#2e9be4" />
                            }
                        </View>
                        <TextInput 
                            placeholder="Contact number (Optional)"
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            onChangeText={text=> this.setState({contact : text})}
                            style={styles.input}
                        />
                    </View>
                    <TouchableOpacity style={styles.button} onPress={()=>{this.create()}}><Text style={styles.btnTxt}>Create</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({ 
    form : {
        marginHorizontal : 15,
        marginTop : 50,
        position : 'absolute',
        flex : 1,
        flexDirection : 'column',
        justifyContent : 'center',
        alignItems : 'center',
        zIndex : 1,
    },
    sport:{
        width : 275,
        borderWidth : 0,
    },
    input:{
        padding : 5,
        width : 275,
        borderWidth : 0,
        margin : 10,
    },
    i:{
        margin : 10,
    },
    box:{
        zIndex : 5,
    },
    flex : {
        flexDirection : 'row',
        alignItems : 'center'
    },
    icon:{
        width : 30,
        height :30,
        alignItems : 'center',
        justifyContent : "center",
    },
    button:{
        backgroundColor : '#2e9be4',
        justifyContent : 'center',
        alignItems : 'center',
        width : 275,
        height : 40,
        borderRadius : 5,
        alignSelf : 'center',
        marginTop : 30,
    },
    btnTxt:{
        fontSize : 25,
        color : '#fff',
        fontWeight : '500',
    },
    error :{
        color : 'red',
    }
});

const sports = ['Cricket','Badminton','Chess','Football','Hockey','Kho-Kho','Basketball',"Volleyball"];

const mapStateToProps = state => ({
    email : state.user.email,
    token : state.user.token,
})

export default connect(mapStateToProps)(New_Match);