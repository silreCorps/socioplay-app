import React from 'react';
import { Text , View , StyleSheet , TouchableOpacity , TextInput} from 'react-native';
import { style } from '../style';
import { connect } from 'react-redux';
import { url } from '../url';

class Change_Password extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            current : "",
            new : "",
            confirm : "",
            err : "",
            message : "",
        }
    }

    change(){
        this.setState({err : ""});
        if(this.state.new != this.state.confirm){
            this.setState({err : "New Password and Confirm Password do not match"});
            return;
        }

        if(this.state.new.length > 0 && this.state.current.length > 0)
        fetch(url+'/reset/password',{
            method : "POST",
            headers : {
                "Authorization" : "Bearer "+this.props.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body : JSON.stringify({
                password : this.state.new,
                current_password : this.state.current,
            })
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200)
                this.setState({message : "Password changed successful"});
            if(res['status']==401)
                this.setState({err : res['result']})
        })
        .catch(err=>{
            console.log(err);
            this.setState({err : "Some error occured please try again"});
        })

        else{
            let str = '';
            if(this.state.new.length == 0) str+="New Password is required ";
            if(this.state.current.length == 0 ) str += "Current Password is required";
            this.setState({err : str})
        }
    }


    render(){
        return (
            <View style={style.container}> 
                <View style={style.header}><Text style={style.header_text}>Change Password</Text></View>
                <View style={styles.container}>
                    {
                        this.state.err.length > 0 ? <Text style={styles.danger}>{this.state.err}</Text> : <View></View>
                    }
                    {
                        this.state.message.length > 0 ? <Text style={styles.msg}>{this.state.message}</Text> : <View></View>
                    }
                    <View style={styles.body}>
                        <TextInput  
                            placeholderTextColor='#A6A4A4'
                            placeholder = "Current Password" 
                            underlineColorAndroid = "#2e9be4"
                            onChangeText={(text)=>{ this.setState({current : text})}}
                            style={styles.input}
                            secureTextEntry={true}/>
                        <TextInput                             
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            placeholder = "New Password" 
                            onChangeText={(text)=>{ this.setState({new : text})}}
                            style={styles.input}
                            secureTextEntry={true}/>
                        <TextInput 
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            placeholder = "Confirm Password" 
                            onChangeText={(text)=>{ this.setState({confirm : text})}}
                            style={styles.input}
                            secureTextEntry={true}/>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.btn} onPress={()=>{this.change();}}><Text style={styles.btntxt}>Change password</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    danger : {
        color : 'red'
    },
    msg:{
        color : '#0098fd'
    },
    body:{
        marginTop : 30,
    },
    container : {
        marginHorizontal : 20,
        marginTop : 30,
    },
    btn:{
        backgroundColor : '#0098fd',
        width : 275,
        justifyContent : "center",
        alignItems : 'center',
        borderRadius : 5,
        height : 40,
        margin : 10,
    },
    btntxt:{
        color : '#fff',
        fontSize : 20,
        fontWeight : "500",
    },
    input:{
        padding : 5,
        width : 275,
        borderWidth : 0,
        margin : 10,
    },
});

const mapStateToProp = state => ({
    token : state.user.token,
    email : state.user.email,
});

export default connect(mapStateToProp)(Change_Password);

