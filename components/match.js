import React from 'react';
import { Text , View , FlatList , Image , StyleSheet ,TouchableOpacity , BackHandler , ScrollView , RefreshControl,ActivityIndicator } from 'react-native';
import {style} from '../style';
import {connect} from 'react-redux';
import { url } from '../url';
import { setChat } from '../actions/notifier';

class Match extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            join : false,
            date_time : '',
            match : {},
            loading : true,
            err : '',
        }
    }

    componentDidMount(){
        BackHandler.addEventListener("hardwareBackPress",()=>{ this.props.navigation.goBack()});
        this.setState({loading : true});
        let id = this.props.navigation.getParam('id');
        fetch(url+'/event/getevent/'+id,{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState((state) => {
                    let obj = {};
                    Object.assign(obj,state);
                    obj.match = res['result'];
                    obj.loading = false;
                    let date = new Date(res['result'].date_time);
                    let hours = date.getHours() > 12 ? (date.getHours()-12) : date.getHours();
                    let sal = date.getHours() >= 12 ? 'PM' : "AM";
                    obj.date_time = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' at ' + hours+':'+date.getMinutes() + ' '+sal;
                    obj.join = res['result'].participants.some(ele => ele.email == this.props.email);
                    obj.err = '';
                    return obj;
                });
            }
            else{
                this.setState({err : 'unable to connect to server , please try again' , loading : false});
            }
        })
        .catch(err=>{
            console.log(err);
            this.setState({err : 'unable to connect to server , please try again' , loading : false});
        })
    }

    _onRefresh(){
        this.setState({loading : true});
        fetch(url+'/event/getevent/'+this.state.match._id,{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState((state) => {
                    let obj = {};
                    Object.assign(obj,state);
                    obj.match = res['result'];
                    obj.loading = false;
                    let date = new Date(res['result'].date_time);
                    let hours = date.getHours() > 12 ? (date.getHours()-12) : date.getHours();
                    let sal = date.getHours() >= 12 ? 'PM' : "AM";
                    obj.date_time = date.getDay() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + ' at ' + hours+':'+date.getMinutes() + ' '+sal;
                    obj.join = res['result'].participants.some(ele => ele.email == this.props.email);
                    obj.err = '';
                    return obj;
                });
            }
            else{
                this.setState({err : 'unable to connect to server , please try again' , loading : false});
            }
        })
        .catch(err=>{
            console.log(err);
            this.setState({err : 'unable to connect to server , please try again' , loading : false});
        })
    }

    leave(id){
        fetch(url+'/event/leaveevent/'+id,{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this._onRefresh();
            }
            else{
                this.setState({err : 'some error occured'})
            }
        })
        .catch(err=>{
            console.log(err);
        });
    }

    join(id){
        fetch(url+'/event/joinevent/'+id,{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this._onRefresh();
            }
            else{
                this.setState({err : 'some error occured'})
            }
        })
        .catch(err=>{
            console.log(err);
        });
    }

    componentWillUnmount(){
        BackHandler.removeEventListener();
    }
    
    render(){

        if(this.state.loading) return (
            <View style={{flex : 1 , justifyContent : "center" , alignItems : "center"}}>
                <ActivityIndicator size = {50} color = "#0098fd" /> 
            </View>
            );
        return (
            <View style={style.container}>
                
                {
                    !this.state.loading &&
                    <View>
                        <View style={style.header}>
                            <Text style={style.header_text}>{this.state.match.sport.toUpperCase()}</Text>
                        </View>
                
                        <ScrollView style={styles.main} refreshControl={
                            <RefreshControl 
                            refreshing={this.state.loading}
                            onRefresh={this._onRefresh.bind(this)}
                            />
                        } >
                        
                            { this.state.err > 0 && 
                                <View>
                                    <Text>{this.state.err}</Text>
                                    <TouchableOpacity onPress={()=>{this._onRefresh()}}><Icon1 name="refresh" size={30}/></TouchableOpacity>
                                </View>
                            }
                            {    this.state.err == 0 && 
                                <View>
                                    <View style={styles.sh}>
                                        <Text style={styles.head}>{this.state.match.sport.toUpperCase()} MATCH</Text>
                                    </View>

                                    <View style={[styles.sh,{marginTop : 20}]}>
                                        <View style={{display : "flex" , flexDirection : "row" , justifyContent : 'space-between'}}>
                                            <Text style={styles.pl}>Players</Text>
                                            <Text style={styles.pl}>{this.state.match.participants.length} coming</Text>
                                        </View>
                                        <FlatList 
                                            horizontal = {true}
                                            data = {this.state.match.participants}
                                            keyExtractor = {(item,index)=>item.email}
                                            renderItem ={({item})=>{
                                                console.log(item);
                                                return (
                                                <Image source = {{uri : item.profile_pic}} style = {styles.image}></Image>
                                            )}
                                            }
                                        />
                                    </View>
                                    
                                    <View style={[styles.sh,{marginTop : 20}]}>
                                    
                                        <Text style={styles.pl2}>Date And Time</Text>
                                        <Text style={styles.txt}>{this.state.date_time}</Text>

                                        <Text style={styles.pl2}>Address</Text>
                                        <Text style={styles.txt}>{this.state.match.address}</Text>
                                        {
                                            this.state.match.contact ?  this.state.match.contact.length !== 0 &&
                                            <View>
                                                <Text style={styles.pl2}>Contact</Text>
                                                <Text>{this.state.match.contact}</Text>
                                            </View>
                                            : <View></View>
                                        }
                                        {
                                            this.state.match.note.length !== 0 &&
                                            <View>
                                                <Text style={styles.pl2}>Note</Text>
                                                <Text>{this.state.match.note}</Text>
                                            </View>
                                        }
                                    </View>

                                    {!this.state.join && <TouchableOpacity style={styles.bt1} onPress={()=>{this.join(this.state.match._id)}}><Text style={styles.bt1txt}>JOIN</Text></TouchableOpacity>}
                                    {this.state.join && 
                                        <View style={styles.btngrp}>
                                            <TouchableOpacity style={styles.bt3} onPress={()=>{this.props.dispatch(setChat({_id : this.state.match._id , sport : this.state.match.sport}));this.props.navigation.navigate('messages')}}><Text style={styles.btntxt}>MESSAGE</Text></TouchableOpacity>
                                            {
                                                this.state.match.organizer == this.props.email ?
                                                <TouchableOpacity style={styles.bt3} onPress={()=>{this.props.navigation.navigate('edit',{id : this.state.match._id})}}><Text style={styles.btntxt}>EDIT</Text></TouchableOpacity>
                                                :
                                                <TouchableOpacity style={styles.bt2} onPress={()=>{this.leave(this.state.match._id)}}><Text style={styles.btntxt}>LEAVE</Text></TouchableOpacity>
                                            }
                                        </View>
                                    }
                                </View>
                            }
                        </ScrollView>
                    </View>
                }
            </View>
        );
    }

}

mapStateToProps = state => ({
    email : state.user.email,
    token : state.user.token,
});

const styles = StyleSheet.create({
    main:{
        margin : 20,
    },
    image : {
        height : 40,
        width : 40,
        borderRadius : 20,
        marginRight : 10,
    },
    pl : {
        fontSize : 15,
        fontWeight : '500',
        marginBottom : 10,
    },
    pl2 : {
        fontSize : 15,
        fontWeight : '500',
    },
    sh : {
        shadowColor : '#A6A4A4',
        shadowRadius : 10,
        shadowOpacity : 1,
        shadowOffset : {
            width : 5,
            height : 5,
        },
        elevation : 5,
        backgroundColor : '#fff',
        padding : 10,
        borderRadius : 5,
    },
    txt : {
        marginBottom : 20,
    },
    bt1:{
        marginTop : 30,
        backgroundColor : '#0098fd',
        alignSelf : 'center',
        height : 40,
        borderRadius : 7,
        width : 320,
        justifyContent : "center",
        alignItems : 'center',
    },
    bt1txt : {
        fontSize : 25,
        fontWeight : '600',
        color : 'white',
    },
    bt2:{
        backgroundColor : "red",
        width : 140,
        justifyContent : "center",
        alignItems : 'center',
        borderRadius : 5,
        height : 30,
    },
    bt3:{
        backgroundColor : '#0098fd',
        width : 140,
        justifyContent : "center",
        alignItems : 'center',
        borderRadius : 5,
        height : 30,
    },
    btntxt:{
        color : '#fff',
        fontSize : 20,
        fontWeight : "500",
    },
    btngrp:{
        alignSelf : 'center',
        marginTop : 30,
        flexDirection : 'row',
        width : 320,
        justifyContent : "space-between"
    },
    head :{
        fontSize : 18,
        alignSelf : 'center',
        fontWeight : '500'
    }

})

export default connect(mapStateToProps)(Match);