import React from 'react';
import { Text , View ,StyleSheet , AsyncStorage , TextInput , TouchableOpacity , Image } from 'react-native';
import { url } from '../url';
import { auth_token,user_info } from '../actions/user';
import { connect } from 'react-redux';

class Signup extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            email : '',
            password : '',
            first_name : '',
            last_name : '',
            err : "",
            loading : '',
        }
    }

    signup(){
        if(this.state.email.length != 0 , this.state.password.length != 0 , this.state.last_name.length != 0 && this.state.first_name.length != 0){
            this.setState({loading : true});
            fetch(url+'/users/register',{
                method : "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body : JSON.stringify({
                    email : this.state.email,
                    password : this.state.password,
                    first_name : this.state.first_name,
                    last_name : this.state.last_name 
                })
            })
            .then(res=>res.json())
            .then(res=>{
                console.log(res);
                if(res['status']==200){
                    AsyncStorage.multiSet([['token',res['result']],['email',this.state.email],['first_name',this.state.first_name],['last_name',this.state.last_name]],(err)=>{
                        if(err){
                            console.log(err);
                            this.setState({err : 'unable to connect to server please try again',loading : false});
                        }
                        else{
                            this.props.dispatch(auth_token(res['result']));
                            this.props.dispatch(user_info({email : this.state.email}));
                            this.props.navigation.navigate('Tab');
                        }
                    });
                }
                else if(res['status']==403){
                    this.setState({err : 'A account with this email id already exist' , loading : false});
                }
                else{
                    this.setState({err : 'Some error occured please try again' , loading : false})
                }
            })
            .catch(err=>{
                console.log(err);
                this.setState({err : 'some error occured please try again',loading : false});
            });
        }

        else{
            this.setState({err : 'some error',loading : false});
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Image source={{uri : 'https://images.pexels.com/photos/556661/pexels-photo-556661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'}} style={styles.backgroundVideo}/>
                <Text style={styles.socioplay}>SocioPlay</Text>
                <View style={styles.a}>
                    <View style={styles.login_container}>
                        <Text style={styles.error}>{this.state.err}</Text>
                        <TextInput textContentType="emailAddress" autoCapitalize="none" onChangeText={text=>{this.setState({email : text})}} placeholder="Email id" underlineColorAndroid = "transparent" placeholderTextColor="#666" style={styles.input} keyboardType="email-address" ></TextInput>
                        <TextInput secureTextEntry={true} textContentType="password" autoCapitalize="none" onChangeText={text=>{this.setState({password : text})}} placeholder="Password" underlineColorAndroid = "transparent" placeholderTextColor="#666" style={styles.input} ></TextInput>
                        <View style={styles.name}>
                            <TextInput textContentType="name" onChangeText={text=>{this.setState({first_name : text})}} placeholder="First Name" underlineColorAndroid = "transparent" placeholderTextColor="#666" style={[styles.input1,{marginRight : 6}]} ></TextInput>
                            <TextInput textContentType="familyName" onChangeText={text=>{this.setState({last_name : text})}} placeholder="Last Name" underlineColorAndroid = "transparent" placeholderTextColor="#666" style={styles.input1} ></TextInput>
                        </View>
                        <TouchableOpacity style={styles.lgbtn} onPress={()=>{this.signup()}}><Text style={styles.log}>Sign Up</Text></TouchableOpacity>
                        <View style={styles.b}>
                            <Text style={styles.othopt} onPress={()=>{this.props.navigation.navigate('social_login')}}>Continue socially   </Text><Text style={styles.othopt} onPress={()=>{this.props.navigation.navigate('login')}}>    Login</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    error : {
        color : 'red',
        padding : 5,
        margin : 10,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    login_container :{ 
        marginHorizontal : 100,
        marginVertical : 30,
    },
    input : {
        width : 274,
        fontSize : 15,
        color : 'black',
        borderWidth : 1,
        borderColor : '#777',
        margin : 10,
        borderRadius : 5,
        padding : 5,
    },
    input1  : {
        width : 134,
        fontSize : 15,
        color : 'black',
        borderWidth : 1,
        borderColor : '#777',
        borderRadius : 5,
        padding : 5,
    },
    a : {
        backgroundColor : 'rgba(255, 255, 255, 0.6)',
        borderRadius : 5,
    },
    lgbtn : {
        margin : 10,
        backgroundColor : '#4F7CDD',
        height : 40,
        borderRadius : 5,
        alignItems : "center",
        justifyContent : "center",
    },
    log : {
        fontSize : 25,
        color : 'white',
    },
    b :{
        margin : 10,
        flexDirection : "row",
        justifyContent : "center"
    },
    othopt : {
        fontSize : 15,
        fontWeight : "500",
        color : '#3B5998'
    },
    socioplay : {
        padding : 10,
        fontSize : 50,
        color : '#fff',
        fontWeight : '600',
    },
    name : {
        flexDirection : "row",
        margin : 10
    }
});

export default connect()(Signup);