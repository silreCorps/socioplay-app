import React from 'react';
import { Text , View ,StyleSheet , TextInput , TouchableOpacity , Image , AsyncStorage } from 'react-native';
import {connect} from 'react-redux';
import {auth_token , user_info} from '../actions/user';
import { url } from '../url';

class Login extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            username : '',
            password : '',
            loading : false,
            err : '',
        }
        console.log(this.props.navigation);
    }

    login(){
        if(this.state.username.length != 0 && this.state.password.length != 0){
            this.setState({loading : true});
            fetch(url+'/users/login',{
                method : "POST", 
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body : JSON.stringify({
                    email : this.state.username,
                    password : this.state.password,
                })
            })
            .then(res=>res.json())
            .then(res=>{
                console.log(res);
                if(res['status']==200){
                    AsyncStorage.multiSet([['token',res['result']],['email',this.state.username]],(err)=>{
                        if(err){
                            console.log(err);
                            this.setState({err : 'unable to connect to server please try again' , loading : false});
                        }
                        else{
                            console.log('success');
                            this.props.dispatch(auth_token(res['result']));
                            this.props.dispatch(user_info({email : this.state.username}));
                            this.props.navigation.navigate('Tab');
                        }
                    });
                }
                else if(res['status']==401){
                    this.setState({err : 'Incorrect email or password',loading : false});
                }
                else if(res['status']==404){
                    this.setState({err : 'Requested user not found',loading  : false});
                }
                else{
                    this.setState({err : 'Some internal server error occured please try again later',loading : false});
                }
            })
            .catch(err=>{
                console.log(err);
                this.setState({err : 'some error occured please login again' , loading : false});
            })
        }
        else{
            if(this.state.password.length == 0) this.setState({err : 'please enter password'});
            else if(this.state.username.length == 0) this.setState({err : 'please enter a email'});
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Image source={{uri : 'https://images.pexels.com/photos/556661/pexels-photo-556661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'}} style={styles.backgroundVideo}/>
                <Text style={styles.socioplay}>SocioPlay</Text>
                <View style={styles.a}>
                    <View style={styles.login_container}>
                        <Text style = {styles.error}>{this.state.err}</Text>
                        <TextInput textContentType="emailAddress" autoCapitalize="none" placeholder="Email Id" onChangeText={(text)=>{this.setState({username : text})}} underlineColorAndroid = "transparent" placeholderTextColor="#777" style={styles.input} keyboardType="email-address" ></TextInput>
                        <TextInput secureTextEntry={true} textContentType="password" autoCapitalize="none" placeholder="Password" onChangeText={(text)=>{this.setState({password : text})}} underlineColorAndroid = "transparent" placeholderTextColor="#777" style={styles.input} ></TextInput>
                        <TouchableOpacity style={styles.lgbtn} onPress={()=>{this.login()}}><Text style={styles.log}>Login</Text></TouchableOpacity>
                        <View style={styles.b}>
                            <Text style={styles.othopt} onPress={()=>{this.props.navigation.navigate('social_login')}}> Continue socially </Text><Text style={styles.othopt} onPress={()=>{this.props.navigation.navigate('signup')}}>  Sign up</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    error :{
        color : 'red',
        margin : 10,
        padding : 5,
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    login_container :{ 
        marginHorizontal : 100,
        marginVertical : 30,
    },
    input : {
        width : 250,
        fontSize : 15,
        color : 'black',
        borderWidth : 1,
        borderColor : '#777',
        margin : 10,
        borderRadius : 5,
        padding : 5,
    },
    a : {
        backgroundColor : 'rgba(255, 255, 255, 0.6)',
        borderRadius : 5,
    },
    lgbtn : {
        margin : 10,
        backgroundColor : '#4F7CDD',
        height : 40,
        borderRadius : 5,
        alignItems : "center",
        justifyContent : "center",
    },
    log : {
        fontSize : 25,
        color : 'white',
    },
    b :{
        margin : 10,
        flexDirection : "row",
        justifyContent : "center"
    },
    othopt : {
        fontSize : 15,
        fontWeight : "500",
        color : '#3B5998'
    },
    socioplay : {
        padding : 10,
        fontSize : 50,
        color : '#fff',
        fontWeight : '600',
    },
});

export default connect()(Login);