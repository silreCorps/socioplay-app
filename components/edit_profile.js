import React from 'react';
import { Text , View , StyleSheet , TouchableOpacity , TextInput , Image} from 'react-native';
import { style } from '../style';
import { connect } from 'react-redux';
import { url } from '../url';

class Edit_Profile extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            first_name : '',
            last_name : '',
            fav_sport : '',
            location : "",
            image : 'https://cdn.movemeback.com/movemeback/images/noprofile.png',
            loading : false,
        }
    }

    componentDidMount(){
        this.setState({loading : true});
        fetch(url+'/users/getprofile',{
            method : "POST",
            headers : {
                "Authorization" : "Bearer "+this.props.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body : JSON.stringify({
                email : this.props.email
            })
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res['status'] == 200){
                let r = res['result'];
                this.setState({
                    first_name : r.first_name ,
                    last_name : r.last_name,
                    fav_sport : r.favourite_sport,
                    location : r.location,
                    image : r.profile_pic,
                    loading : false,
                });
            }
        })
        .catch(err=>{
            console.log(err);
        })
    }

    update(){
        let s = this.state;
        let c = this.check;
        if(c(s.first_name)&&c(s.last_name)){
            fetch(url+'/users/editprofile',{
                method : "POST",
                headers : {
                    "Authorization" : "Bearer "+this.props.token,
                    "Content-Type": "application/json; charset=utf-8",
                },
                body : JSON.stringify({
                    first_name : s.first_name,
                    last_name : s.last_name,
                    favourite_sport : s.fav_sport,
                    location : s.location
                })
            })
            .then(res=>res.json())
            .then(res=>{
                if(res['status']=200)
                this.props.navigation.navigate('profile');
            })
            .catch(err=>{
                console.log(err);
                this.setState({err : "Some error occured please try again"});
            })
        }
        else{
            let str = "";
            if(!c(s.first_name)) str+="First Name is Required ";
            if(!c(s.last_name)) str+="Last Name is Required";
            this.setState({err : str})
        }
    }

    check(str){
        if(str.length > 0) return true;
        return false;
    }

    render(){
        return (
            <View style={style.container}> 
                <View style={style.header}><Text style={style.header_text}>Edit Profile</Text></View>
                <View style={styles.container}>
                    <View style={styles.row}>
                        <Image source={{uri : this.state.image}} style={styles.image}/>
                        <TouchableOpacity onPress={()=>{}} style={styles.link}>
                            <Text style={styles.blue}>Change Profile Picture</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.body}>
                        <TextInput  
                            placeholderTextColor='#A6A4A4'
                            placeholder = {this.state.first_name.length >0 ? this.state.first_name : "First Name"} 
                            underlineColorAndroid = "#2e9be4"
                            onChangeText={(text)=>{ this.setState({first_name : text})}}
                            style={styles.input}/>
                        <TextInput                             
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            placeholder = {this.state.last_name.length >0 ? this.state.last_name : "Last Name"} 
                            onChangeText={(text)=>{ this.setState({last_name : text})}}
                            style={styles.input}/>
                        <TextInput 
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            placeholder = {this.state.fav_sport.length >0 ? this.state.fav_sport : "Favourite Sport"} 
                            onChangeText={(text)=>{ this.setState({fav_sport : text})}}
                            style={styles.input}/>
                        <TextInput 
                            placeholderTextColor='#A6A4A4'
                            underlineColorAndroid = "#2e9be4"
                            placeholder  = {this.state.location.length >0 ? this.state.location : "Address"} 
                            onChangeText={(text)=>{ this.setState({location : text})}}
                            style={styles.input}/>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.btn} onPress={()=>{this.update();}}><Text style={styles.btntxt}>Update</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

}


const styles = StyleSheet.create({
    link : {
        justifyContent : "center",
        alignItems : 'center',
    },
    body:{
        marginTop : 30,
    },
    row:{
        flexDirection : 'row',
    },
    container : {
        marginHorizontal : 20,
        marginTop : 30,
    },
    image : {
        width : 60,
        height : 60,
        borderRadius : 30,
        paddingRight : "auto",
        marginRight : 40,
    },
    btn:{
        backgroundColor : '#0098fd',
        width : 275,
        justifyContent : "center",
        alignItems : 'center',
        borderRadius : 5,
        height : 40,
        margin : 10,
    },
    btntxt:{
        color : '#fff',
        fontSize : 20,
        fontWeight : "500",
    },
    input:{
        padding : 5,
        width : 275,
        borderWidth : 0,
        margin : 10,
    },
    blue : {
        color : "#0098fd",
        fontSize : 17
    }
})

const mapStateToProp = state => ({
    token : state.user.token,
    email : state.user.email,
});

export default connect(mapStateToProp)(Edit_Profile);

