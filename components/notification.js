import React from 'react';
import { Text , View , StyleSheet , FlatList } from 'react-native';
import {style} from '../style';
import { connect } from 'react-redux';
import { notificationActive } from '../actions/notifier';

class Notifi extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            notif : []
        }
    }

    componentDidMount(){
        this.props.dispatch(notificationActive);
        this.setState({
            notif : this.props.arr.map(ele =>{
                let obj = {};
                let i = 0 ;
                Object.assign(obj,ele);
                if(obj._id){
                    obj.key = obj._id;
                }
                else{
                    obj.key = i.toString();
                    i++;
                }
                return obj;
            })
        });
    }

    render(){
        return (
            <View style={style.container}>                
                <View style={style.header}><Text style={style.header_text}>Notifications</Text></View>
                <View style={styles.mar}>
                    <FlatList  
                    data={this.state.notif}
                    renderItem={({item})=>(
                        <View style = {styles.n}>
                            <Text styles = {styles.txt}>{item.content}</Text>
                        </View>
                    )}/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    mar : {
        margin : 20,
        backgroundColor : 'rgba(225, 242, 253, 0.57)',
        borderRadius : 5,
    },
    n : {
        marginVertical : 5,
        padding : 10,
        borderBottomColor : '#E1F2FD',
        borderBottomWidth : 1,
    },
})

const mapStateToProp = (state)=>{
    return({
        arr : state.notification.notification
    })
}

export default connect(mapStateToProp)(Notifi);