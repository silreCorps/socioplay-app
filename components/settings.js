import React from 'react';
import { Text , View , StyleSheet , TouchableOpacity , AsyncStorage} from 'react-native';
import { style } from '../style';
import { remove_user } from '../actions/user';
import { connect } from 'react-redux';

class Settings extends React.Component {

    constructor(prop){
        super(prop);
    }

    logout(){
        AsyncStorage.removeItem('token',(err)=>{
            console.log(err);
        });
        AsyncStorage.removeItem('email',err=>{
            console.log(err);
        });
        this.props.dispatch(remove_user);
        this.props.navigation.navigate('Logstack');
    }

    render(){
        return (
            <View style={style.container}> 
                <View style={style.header}><Text style={style.header_text}>Settings</Text></View>
                
                <View style = {{marginTop : 30}}>
                    <TouchableOpacity style={styles.tile} onPress={()=>{this.props.navigation.navigate('change_password')}}><Text style={styles.tileText}>change password</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.tile} onPress={()=>{this.props.navigation.navigate('edit_profile')}}><Text style={styles.tileText}>Edit Profile</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.tile} onPress={()=>{this.props.navigation.navigate('privacy_policy')}}><Text style={styles.tileText}>privacy policy</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.tile} onPress={()=>{this.props.navigation.navigate('terms_and_condition')}}><Text style={styles.tileText}>Terms and condition</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.tile} onPress={()=>{this.logout()}}><Text style={styles.tileText}>Logout</Text></TouchableOpacity>
                </View>
                
            </View>
        );
    }

}

const styles = StyleSheet.create({
    tile : {
        paddingVertical : 5,
        paddingHorizontal : 10,
        backgroundColor : "#f9f9f9",
        marginVertical : 5,
    },
    tileText : {
        fontSize : 16,
        color : "#0098fd"
    }
})

export default connect()(Settings);

