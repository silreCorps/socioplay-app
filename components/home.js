import React from 'react';
import { Text , View , TextInput , StyleSheet,TouchableOpacity,FlatList,ScrollView,TouchableWithoutFeedback,Image,RefreshControl , AsyncStorage , ActivityIndicator } from 'react-native';
import { style } from '../style';
import { connect } from 'react-redux';
import { url } from '../url';
import Icon from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import { set_user_name } from '../actions/user';
const sports = ["Badminton","Cricket","Chess","Kho-Kho","Kabbadi","Hockey","Boxing","Football","Basketball","Volleyball","Tennis","Table Tennis"];

class Home extends React.PureComponent {

    constructor(prop){
        super(prop);
        this.state = {
            done : true,
            query : '',
            focused : false,
            predictions : [],
            data : [],
            loading : false,
            initial : false,
            err : '',
        }
    }

    predict(){
        let q = this.state.query;
        let arr = sports.filter(ele => ele.toLowerCase().indexOf(q.toLowerCase()) != -1).splice(0,5);
        this.setState((prev = this.state)=>{
            let obj={};
            Object.assign(obj,prev);
            obj.predictions.splice(0,obj.predictions.length);
            arr.forEach(ele=>{
                obj.predictions.push({key : ele});
            });
            return obj;
        });
    }

    componentDidMount(){
        this.setState({initial : true});
        fetch(url+'/event/getallevents',{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState({
                    data : res['result'].map(ele=>{
                        let obj = {};
                        Object.assign(obj,ele);
                        let date = new Date(obj.date_time);
                        obj.profile_dip = ele.participants.length > 4 ? 4 : ele.participants.length;
                        obj['date'] = date.toLocaleDateString();
                        let hours = date.getHours() > 12 ? (date.getHours()-12) : date.getHours();
                        let sal = date.getHours() >= 12 ? 'PM' : "AM";
                        obj['time'] =  hours+':'+date.getMinutes() + ' '+sal;
                        obj.err = '';
                        return obj;
                    }),
                    initial : false,
                });
            }
            else{
                this.setState({err : 'unable to connect to server , please try again' , loading : false});
            }
        })
        .catch(err=>{
            console.log(err);
        });

        // get user name and store it in asyncstorage

        fetch(url+'/users/name',{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                AsyncStorage.setItem('name',res['result'],(err)=>{
                    if(err){
                        console.log(err);
                        return;
                    }
                    set_user_name(res['result']);
                })
            }
            else{
                console.log('err');
            }
        })
        .catch(err=>{
            console.log(err);
        });
    }

    _onRefresh(){
        this.setState({loading : true});
        fetch(url+'/event/getallevents',{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState({data : res['result'].map(ele=>{
                    let obj = {};
                    Object.assign(obj,ele);
                    let date = new Date(obj.date_time);
                    obj.profile_dip = ele.participants.length > 4 ? 4 : ele.participants.length;
                    obj['date'] = date.toLocaleDateString();
                    let hours = date.getHours() > 12 ? (date.getHours()-12) : date.getHours();
                    let sal = date.getHours() >= 12 ? 'PM' : "AM";
                    obj['time'] = hours+':'+date.getMinutes() + ' '+sal;
                    obj.err = '';
                    return obj;
                    }),
                    loading : false,
                });
            }
            else{
                this.setState({err : 'unable to connect to server , please try again' , loading : false});
            }
        })
        .catch(err=>{
            console.log(err);
        })
    }

    filteredEvents(){
        fetch(url+'/event/getfilteredevents?sport='+this.state.query,{
            headers : {
                "Authorization" : 'bearer '+this.props.token,
            }
        })
        .then(res=>res.json())
        .then(res=>{
            if(res['status']==200){
                this.setState({data : res['result'].map(ele=>{
                    let obj = {};
                    Object.assign(obj,ele);
                    let date = new Date(obj.date_time);
                    obj.profile_dip = ele.participants.length > 4 ? 4 : ele.participants.length;
                    obj['date'] = date.toLocaleDateString();
                    obj['time'] = date.toLocaleTimeString({hour12 : true});
                    obj.err = '';
                    return obj;
                    })
                });
            }
            else{
                this.setState({err : 'unable to connect to server , please try again' , loading : false});
            }
        })
        .catch(err=>{
            console.log(err);
        })
    }

    render(){

        if(this.state.initial) return (
            <View style={{flex : 1 , justifyContent : "center" , alignItems : "center"}}>
                <ActivityIndicator size = {50} color = "#0098fd" /> 
            </View>
            );

        return (
            <View style={style.container}>
                <View style={style.header}><Text style={style.header_text}>Home</Text></View>
                <TouchableWithoutFeedback onPress={()=>{this.setState({done : true})}}>
                    <ScrollView
                        refreshControl={
                            <RefreshControl 
                            refreshing={this.state.loading}
                            onRefresh={this._onRefresh.bind(this)}
                            />
                        } 
                        style={styles.home} 
                        keyboardShouldPersistTaps='handled'
                    >
                        <View style={styles.ok} >
                            <TextInput 
                                onChangeText={(text)=>{
                                    this.setState({query : text , done : false});
                                    this.predict();
                                }}  
                                placeholder="What do you want to play ?" 
                                placeholderTextColor="#A6A4A4"
                                style={styles.query}
                                value = {this.state.query}
                                onSubmitEditing = {()=>{this.setState({done : true})}}
                                underlineColorAndroid = "#0098fd"
                            ></TextInput>
                            <TouchableOpacity onPress={()=>{this.filteredEvents()}}><Icon name="search" color = "#0098fd" size = {20}></Icon></TouchableOpacity>
                        </View>


                        {
                            this.state.predictions.length != 0 && !this.state.done &&
                            <FlatList 
                                style={styles.predictor_view}
                                data={this.state.predictions}
                                extraData = {this.state}
                                renderItem = {
                                    ({item})=>(
                                            <TouchableOpacity 
                                            style={styles.predictions} 
                                            onPress={()=>{
                                                console.log('ok');
                                                this.setState({query : item.key,done : true});
                                                console.log(this.state);
                                            }}>
                                                <Text>{item.key}</Text>
                                            </TouchableOpacity>
                                    )
                                }
                            />
                        }


                        { this.state.err > 0 && 
                            <View>
                                <Text>{this.state.err}</Text>
                                <TouchableOpacity onPress={()=>{this._onRefresh()}}><Icon1 name="refresh" size={30}/></TouchableOpacity>
                            </View>
                        }


                        <FlatList
                            style={{marginTop : 30}}  
                            extraData={this.state}
                            data={this.state.data}
                            keyExtractor = {(item)=>item._id} 
                            renderItem = {({item})=>(
                                <TouchableOpacity 
                                style={styles.feed}
                                onPress={()=>{
                                        this.props.navigation.navigate('match',{id : item._id});
                                    }}>
                                    <View style={[styles.ok,styles.bor]}>
                                        <Image source={{uri : item.image}} style={styles.image}/>
                                        <Text style={[styles.add,styles.bor1]}><Text style={{fontWeight : '500'}}>{item.sport.toUpperCase()}</Text> at {item.address.slice(0,40)}</Text>
                                        <View style={styles.fine}>
                                            <Text style={[styles.small,styles.bottom]}>{item.date}</Text>
                                            <Text style={styles.small}>{item.time}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.ok}>
                                        <Text>Players : </Text>
                                        <View style={styles.add1}>
                                            {item.profile_dip >= 1 && <Image source={{uri : item.participants[0].profile_pic}} style={styles.image1} />}
                                            {item.profile_dip >= 2 && <Image source={{uri : item.participants[1].profile_pic}} style={styles.image1} />}
                                            {item.profile_dip >= 3 && <Image source={{uri : item.participants[2].profile_pic}} style={styles.image1} />}
                                            {item.profile_dip >= 4 && <Image source={{uri : item.participants[3].profile_pic}} style={styles.image1} />}
                                        </View>
                                        <Text>{item.participants.length - item.profile_dip > 0 ? '+ '+(item.participants.length - item.profile_dip)+' more coming' : ""}</Text>
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                    </ScrollView>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bottom : {
        borderBottomColor : '#0098fd',
        borderBottomWidth : 1,
    },
    pla:{
        margin : 5
    },
    feed : {
        flexDirection : 'column',
        marginBottom : 20,
        height : 80,
    },
    add : {
        width : 230,
        paddingLeft : 5,
    },
    add1 : {
        width : 160,
        paddingLeft : 5,
        flexDirection : 'row',
        marginTop : 5,
    },
    bor : {
        borderWidth : 1,
        borderColor : '#0098fd',
    },
    bor1:{
        borderLeftWidth : 1,
        borderRightWidth : 1,
        borderColor : '#0098fd',
    },
    image : {
        height : 40,
        width : 40,
        borderRadius : 5
    },
    image1 : {
        height : 30,
        width : 30,
        borderRadius : 15,
        marginRight : 10,
    },
    small : {
        fontSize : 10,
    },
    fine : {
        width : 50,
        flexDirection : 'column',
        alignItems : 'center'
    },
    ok:{
        flexDirection : 'row',
        alignItems : 'center',
        borderRadius : 5
    },
    home : {
        margin : 20,
        minHeight : 400,
        height : 600,
    },
    query : {
        width : 250,
        padding : 5,
        marginRight : 10,
    },
    predictions : {
        marginVertical : 5,
        borderBottomColor : '#0098fd'
    },
    predictor_view : {
        width : 200,
        marginLeft : 20,
        zIndex: 10,
        shadowColor : '#A6A4A4',
        shadowRadius : 10,
        shadowOpacity : 1,
        shadowOffset : {
            width : 5,
            height : 5,
        },
        elevation : 5,
        backgroundColor : '#fff',
        padding : 5,
        position : 'absolute',
        top : 35,
    },
})

const mapStateToProp = (state)=>({
    token : state.user.token
});
export default connect(mapStateToProp)(Home);