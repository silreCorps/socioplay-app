import React from 'react';
import { AsyncStorage , TouchableOpacity , View ,Image,StyleSheet , Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {auth_token , user_info, set_user_name} from '../actions/user';
import { url } from '../url';

class Social_Login extends React.Component {

    constructor(prop){
        super(prop);
        AsyncStorage.multiGet(['token','email','name'],(err,res)=>{
            if(err || !res[0][1]){
                console.log(err);
                return;
            }
            else{
                fetch(url+'/users/verifytoken',{
                    headers : {
                        "Authorization" : "Bearer "+res[0][1],
                    }
                })
                .then(resp=>{
                    if(resp['status']==200){
                        this.props.dispatch(auth_token(res[0][1]));
                        this.props.dispatch(user_info({email : res[1][1]}));
                        this.props.dispatch(set_user_name(res[2][1]));
                        this.props.navigation.navigate('Tab');
                    }
                })
                .catch(err=>{
                    console.log(err);
                    return;
                });
            }
        })
    }

    render(){
        return (
            <View style={styles.container}>
                {/* <Video source={vid} posterSource={{uri : 'https://images.pexels.com/photos/556661/pexels-photo-556661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'}} style={styles.backgroundVideo} shouldPlay isLooping isMuted={true} resizeMode = {Expo.Video.RESIZE_MODE_STRETCH}/> */}
                <Image source={{uri : 'https://images.pexels.com/photos/556661/pexels-photo-556661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'}} style={styles.backgroundVideo}/>
                <Text style={styles.socioplay}>SocioPlay</Text>
                <View style={styles.a}>
                    <View style={styles.social_container}>
                        <TouchableOpacity onPress={()=>{ this.props.navigation.navigate('Tab')}} style={[styles.social_btn,styles.google]}> 
                            <Text style={styles.text}><Icon style={styles.pad} name = 'google' color = 'white' size = {23}/>  Continue with Google</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{ this.props.navigation.navigate('Tab')}} style={[styles.social_btn,styles.facebook]}> 
                            <Text style={styles.text}><Icon style={styles.pad} name = 'facebook' color = 'white' size = {23}/>  Continue with Facebook</Text>
                        </TouchableOpacity>
                        <View style={styles.b}>
                            <Text style={styles.log} onPress={()=>{this.props.navigation.navigate('login')}}>Login  </Text><Text style={styles.log} onPress={()=>{this.props.navigation.navigate('signup')}}>  Sign up</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor : 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    a : {
        backgroundColor : 'rgba(255, 255, 255, 0.6)',
        borderRadius : 5,
    },
    social_btn : {
        paddingVertical : 10,
        width : 230,
        height : 50,
        borderRadius : 5,
        marginHorizontal : 50,
    },
    google:{
        backgroundColor : '#db3236'
    },
    facebook : {
        marginTop : 30,
        backgroundColor : '#3B5998'
    },
    social_container : {
        margin : 40,
    },
    text : {
        color : 'white',
        fontSize : 15,
        textAlign : 'center',
    },
    b :{
        margin : 10,
        flexDirection : "row",
        justifyContent : "center"
    },
    log :{
        fontSize : 15,
        fontWeight : "500",
        color : '#3B5998'
    },
    socioplay : {
        padding : 10,
        fontSize : 50,
        color : '#fff',
        fontWeight : '600',
    },
    pad :{
        paddingTop : 5,
    }
});

export default connect()(Social_Login);