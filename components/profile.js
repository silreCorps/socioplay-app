import React from 'react';
import { Text , View , StyleSheet , TouchableOpacity , AsyncStorage , Image , ScrollView , FlatList , RefreshControl , ActivityIndicator , Dimensions} from 'react-native';
import { style } from '../style';
import { connect } from 'react-redux';
import { url } from '../url';
import Icon from 'react-native-vector-icons/Feather';

class Matches extends React.Component{ 

    constructor(prop){
        super(prop);
        this.state = { data : []};
    }

    componentDidMount(){
        let res = this.props.events;
        this.setState({data : res.map(ele=>{
            let obj = {};
            Object.assign(obj,ele);
            let date = new Date(obj.date_time);
            let hours = date.getHours() > 12 ? (date.getHours()-12) : date.getHours();
            let sal = date.getHours() >= 12 ? 'PM' : "AM";
            obj.date = date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear() + '  ' + hours+':'+date.getMinutes() + ' '+sal;
            return obj;
            })
        });   
    }

    render(){
        if(this.state.data.length == 0){
            return(<View></View>);
        }
        return(
            <View>
                <FlatList
                    style={{marginTop : 30 , marginHorizontal : 20}}  
                    extraData={this.state}
                    data={this.state.data}
                    keyExtractor = {(item)=>item._id} 
                    renderItem = {({item})=>(
                        <TouchableOpacity 
                        style={styles.feed}
                        onPress={()=>{
                                this.props.navigation.navigate('match',{id : item._id});
                            }}>
                            <View style={styles.ok1}>
                                <Image source={{uri : item.image}} style={styles.image1}/>
                                <View>
                                    <Text><Text style={{fontWeight : '500'}}>{item.sport.toUpperCase()}</Text> at {item.address.slice(0,40)}</Text>
                                    <Text style={styles.small}>{item.date}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                />
                <View style={{marginBottom  : 30}}></View>
           </View>
        )}
}

class Friends extends React.Component {
    render(){
        return (
        <View>
            <FlatList
                    style={{marginTop : 30 , marginHorizontal : 20}}  
                    extraData={this.props}
                    data={this.props.friends}
                    keyExtractor = {(item)=>item.email} 
                    renderItem = {({item})=>(
                        <TouchableOpacity 
                            style={styles.feed}
                            onPress={()=>{
                                this.props.navigation.navigate('profile',{id : item._id});
                            }}>
                            <View style={styles.ok1}>
                                <Image source={{uri : item.profile_pic}} style={styles.image1}/>
                                <Text style={{fontWeight : '500' , fontSize : 18}}>{item.name}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
                <View style={{marginBottom  : 30}}></View>
        </View>
    )}
}

class Profile extends React.Component {

    constructor(prop){
        super(prop);
        this.state = {
            name : '',
            fav_sport : '',
            location : "",
            image : 'https://cdn.movemeback.com/movemeback/images/noprofile.png',
            events : [],
            friends : [],
            loading : false,
            tab : 0,
            initial : false
        }
    }

    componentDidMount(){
        this.setState({initial : true});
        fetch(url+'/users/getprofile',{
            method : "POST",
            headers : {
                "Authorization" : "Bearer "+this.props.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body : JSON.stringify({
                email : this.props.email
            })
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res['status'] == 200){
                let r = res['result'];
                this.setState({
                    name : r.first_name +' '+ r.last_name,
                    fav_sport : r.favourite_sport,
                    location : r.location,
                    image : r.profile_pic,
                    events : r.events.reverse(),
                    friends : r.friends,
                    initial : false,
                })
            }
        })
    }

    refresh(){
        this.setState({loading : true});
        fetch(url+'/users/getprofile',{
            method : "POST",
            headers : {
                "Authorization" : "Bearer "+this.props.token,
                "Content-Type": "application/json; charset=utf-8",
            },
            body : JSON.stringify({
                email : this.props.email
            })
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res['status'] == 200){
                let r = res['result'];
                this.setState({
                    name : r.first_name +' '+ r.last_name,
                    fav_sport : r.favourite_sport,
                    location : r.location,
                    image : r.profile_pic,
                    events : r.events.reverse(),
                    friends : r.friends,
                    loading : false,
                })
            }
        })
    }

    render(){
        if(this.state.initial) return (
            <View style={{flex : 1 , justifyContent : "center" , alignItems : "center"}}>
                <ActivityIndicator size = {50} color = "#0098fd" /> 
            </View>
            );
        return (
            <View style={style.container}> 
                <View style={[style.header,{flexDirection : 'row' , justifyContent : "space-between" , alignItems : "center"}]}>
                    <View></View>
                    <Text style={{fontSize : 18 , color : "#0098fd"}}>Profile</Text>
                    <Icon.Button name="settings" backgroundColor="transparent" size={20} color="#0098fd" onPress={()=>{this.props.navigation.navigate('settings')}} />
                </View>
                <ScrollView style={styles.mar}
                refreshControl={
                    <RefreshControl 
                    refreshing={this.state.loading}
                    onRefresh={this.refresh.bind(this)}
                    />
                } >
                    <View style={styles.top}>
                        <View style={styles.header}>
                            <Image source={{uri : this.state.image}} style={styles.image} />
                            <View style={styles.ok}>
                                <Text style={styles.heading}>{this.state.events.length}</Text>
                                <Text style={styles.bottom}>Matches</Text>
                            </View>
                            <View style={styles.ok}>
                                <Text style={styles.heading}>{this.state.friends.length}</Text>
                                <Text style={styles.bottom}>Friends</Text>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.bold}>{this.state.name}</Text>
                            <Text>Likes to play {this.state.fav_sport}</Text>
                        </View>
                    </View>
                    <View style={styles.body}>
                        <View style={styles.oyeoye}>
                            <TouchableOpacity onPress={()=>{this.setState({tab : 0})}} style={[styles.pad,styles.right]}><Text style={this.state.tab == 0 ? styles.balebale : styles.shavashava}>Matches</Text></TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this.setState({tab : 1})}} style={styles.pad}><Text style={this.state.tab == 1 ? styles.balebale : styles.shavashava}>Friends</Text></TouchableOpacity>
                        </View>
                        <View>
                            {
                                this.state.tab == 0 ? <Matches events = {this.state.events} navigation = {this.props.navigation} /> : <Friends friends = {this.state.friends}  navigation = {this.props.navigation}/>
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    bottom : {
        borderBottomColor : '#0098fd',
        borderBottomWidth : 1,
    },
    small : {
        fontSize : 10,
    },
    feed : {
        flexDirection : 'column',
        marginBottom : 20,
        height : 80,
    },
    image : {
        height : 40,
        width : 40,
        borderRadius : 5
    },
    bor1:{
        borderLeftWidth : 1,
        borderRightWidth : 1,
        borderColor : '#0098fd',
    },
    bor : {
        borderWidth : 1,
        borderColor : '#0098fd',
    },
    ok1:{
        flexDirection : 'row',
        alignItems : 'center',
        borderRadius : 5
    },
    set:{
        alignSelf : "flex-end"
    },
    right : {
        borderRightWidth : 1,
        borderRightColor : 'rgba(225,242,253,0.61)'
    },
    mar :{ 
        marginTop : 20,
    },
    image : {
        width : 60,
        height : 60,
        borderRadius : 30,
        paddingRight : "auto",
        marginRight : 40,
    },
    image1 : {
        width : 60,
        height : 60,
        borderRadius : 30,
        paddingRight : "auto",
        marginRight : 20,
    },
    top : {
        paddingBottom : 20,
        paddingHorizontal : 20,
        borderBottomWidth : 1,
        borderBottomColor : 'rgba(225, 242, 253, 0.61)',
    },
    header :{
        flexDirection : 'row',
        paddingBottom : 15,
    },
    ok : {
        marginHorizontal : 20,
    },
    heading : {
        fontSize : 30,
        fontWeight : '500',
        color : "#0098fd",
        alignSelf : "center"
    },
    bottom : {
        alignSelf : 'center'
    },
    bold : {
        fontWeight : '500'
    },
    body:{
        marginBottom : 10
    },
    oyeoye:{
        flexDirection : 'row',
        justifyContent : 'space-evenly',
        borderBottomWidth : 1,
        borderBottomColor : 'rgba(225, 242, 253, 0.61)',
        borderRadius : 5
    },
    balebale:{
        fontSize : 16,
        color : '#0098fd',
        alignSelf : "center"
    },
    shavashava : {
        fontSize : 16,
        alignSelf : "center"
    },
    pad : {
        flex : 1,
        paddingVertical : 10,
    },
})


const mapStateToProp = state => ({
    token : state.user.token,
    email : state.user.email,
});

connect()(Matches);
connect()(Friends);
export default connect(mapStateToProp)(Profile);

