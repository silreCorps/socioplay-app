import { createStackNavigator } from 'react-navigation'; 
import profile from '../components/profile';
import edit_profile from '../components/edit_profile';
import settings from '../components/settings';
import privacy_policy from '../components/privacy_policy';
import change_password from '../components/change_password';
import terms_and_condition from '../components/terms_and_condition';
import match from '../components/match';

const profile_nav = createStackNavigator({
    profile : {
        screen : profile
    },
    edit_profile : {
        screen : edit_profile
    },
    settings : {
        screen : settings
    },
    privacy_policy : {
        screen : privacy_policy
    },
    change_password : {
        screen : change_password
    },
    terms_and_condition : {
        screen : terms_and_condition
    },
    match : {
        screen : match
    },
},{
    initialRouteName : 'profile',
    headerMode : 'none'
});

export default profile_nav;