import { createStackNavigator } from 'react-navigation';
import Social_Login from '../components/social_login';
import Login from '../components/login';
import Signup from '../components/signup';

const AuthStack = createStackNavigator({
    social_login : {
        screen : Social_Login,
    },
    login : {
        screen : Login,
    },
    signup : {
        screen : Signup,
    }
},{
    initialRouteName : 'social_login',
    headerMode : 'none'
});

export default AuthStack;