import { createStackNavigator } from 'react-navigation'; 
import match from '../components/new_match';
import match_nav from './match_stack';

const newm = createStackNavigator({
    main : {
        screen : match
    },
    show : {
        screen : match_nav
    },
},{
    initialRouteName : 'main',
    headerMode : 'none'
});

export default newm;