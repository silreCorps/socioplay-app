import { createStackNavigator } from 'react-navigation';
import match_nav from './match_stack';
import joined_matches from '../components/joined_matches';
import messages from '../components/messages';

const home = createStackNavigator({
    main : {
        screen : joined_matches
    },
    match : {
        screen : match_nav
    },
    message : {
        screen : messages
    }

},{
    initialRouteName : 'main',
    headerMode : 'none'
});

export default home;