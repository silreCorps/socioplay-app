import { createStackNavigator } from 'react-navigation';
import match from '../components/match';
import edit_match from '../components/edit_match';
import messages from '../components/messages';

const match_nav = createStackNavigator({
    match : {
        screen : match
    },
    edit : {
        screen : edit_match
    },
    messages : {
        screen : messages
    }
},{
    initialRouteName : 'match',
    headerMode : 'none'
});

export default match_nav;