import { createStackNavigator } from 'react-navigation'; 
import Home from '../components/home';
import match_nav from './match_stack';

const home = createStackNavigator({
    main : {
        screen : Home
    },
    match : {
        screen : match_nav
    },
},{
    initialRouteName : 'main',
    headerMode : 'none'
});

export default home;