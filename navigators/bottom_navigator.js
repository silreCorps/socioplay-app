import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import home from './homeNavigator';
import Notifi from '../components/notification';
import Messages from './starred_navigator';
import newm from './newMatch';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/FontAwesome'
import profile_nav from './profile_navigator';
import { newNotification, storeNotification } from '../actions/notifier';

import { connect } from 'react-redux';
import io from 'socket.io-client';
import {url} from '../url';

/*
class message extends React.Component{

    constructor(prop){
        super(prop);
        this.state = {
            msg : false,
        }
    }

    socket = io(url,{
        path : '/messages',
        query : {
            token : this.props.token,
            email : this.props.email,
        }
    });

    componentDidMount(){
        fetch(url+'/users/unreadstatus',{
            headers : {
                "Authorization" : "Bearer "+this.props.token 
            }
        })
        .then(res => res.json())
        .then(res=>{
            console.log(res);
            if(res['status']==200){
                this.setState({msg : res['result']})
            }
            else{
                console.log('what is it after all '+res['result']);
            }
        })
        .catch(err=>{
            console.log('error error '+err);
        });

        this.socket.on('connect',()=>{
            this.socket.on('incoming message',(obj)=>{
                this.setState({msg : true});
            });
        })
    }

    componentWillUnmount(){
        this.socket.disconnect();
    }

    render(){
        if(this.state.msg){
            return (
                <Icon2 name="star" size={this.props.horizontal ? 20 : 25} color="red" />
            );
        }
        else{
            return(
                <Icon2 name="star-o"  size={this.props.horizontal ? 20 : 25} color={this.props.color} />
            )
        }
    }

}

const mapStateToProps = state => {
    console.log(state);
    return ({
        id : state.notification.id,
        sport : state.notification.sport,
        token : state.user.token,
        email : state.user.email,
        name : state.user.name,
    })
}

const Msg = connect(mapStateToProps)(message);

*/


class notification extends React.Component{
    
    constructor(prop){
        super(prop);

        this.props.dispatch(storeNotification([]));
    }

    socket = io(url,{
        path : '/notification',
        query : {
            token : this.props.token,
            email : this.props.email,
        }
    });

    componentDidMount(){

        this.socket.on('connect',()=>{
            this.socket.on('notification',(obj)=>{
                this.props.dispatch(newNotification(obj));
            })
        });

        fetch(url+'/users/getnotification',{
            headers : {
                "Authorization" : "Bearer "+this.props.token 
            }
        })
        .then(res => res.json())
        .then(res=>{
            if(res['status']==200){
                this.props.dispatch(storeNotification(res['result']));
            }
            else{
                console.log('what is it after all '+res['result']);
            }
        })
        .catch(err=>{
            console.log('error error '+err);
        });
        
    }

    componentWillUnmount(){
        this.socket.disconnect();
    }

    render(){
        if(this.props.new){
            return (
                <Icon2 name="bell" size={this.props.horizontal ? 20 : 25} color="red" />
            );
        }
        else{
            return(
                <Icon2 name="bell-o"  size={this.props.horizontal ? 20 : 25} color={this.props.color} />
            )
        }
    }
}

const mapStateToProps = state => {
    return ({
        token : state.user.token,
        email : state.user.email,
        new : state.notification.new,
    })
}

const Notif = connect(mapStateToProps)(notification);

export default Tabs = createBottomTabNavigator({
    Home : {
        screen : home
    },
    "New Match" : {
        screen : newm
    },
    Messages : {
        screen : Messages
    },
    Notification : {
        screen : Notifi
    },
    Profile : {
        screen : profile_nav
    },
},{
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Home') {
                return (<Icon name = 'home-outline' size={horizontal ? 20 : 25} color={tintColor} ></Icon>)
            } 
            else if (routeName === 'New Match') {
                iconName = `plus`;
            }
            else if (routeName === 'Messages'){
                // return (<Msg horizontal={horizontal} color = {tintColor}/>)
                iconName = 'star';
            }
            else if (routeName === 'Notification'){
                return (<Notif horizontal={horizontal} color = {tintColor}  routeName={routeName} />);
            }
            else if (routeName === 'Profile'){
                iconName = 'user';
            }
            return <Icon1 name={iconName} size={horizontal ? 20 : 25} color={tintColor}/>;
        },
    }),
    initialRouteName : 'Home',
    tabBarOptions: {
        activeTintColor: '#0098fd',
        inactiveTintColor: 'gray',
        showLabel : false,
        activeBackgroundColor : '#FAF9F9',
        inactiveBackgroundColor : '#FAF9F9',
    },
});
