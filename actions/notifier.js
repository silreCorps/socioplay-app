export const setChat = chat => ({
    type : 'SET_CHAT',
    id : chat._id,
    sport : chat.sport,
});

export const newMessage = message => ({
    type : "NEW_MESSAGE",
    id : message.id
});

export const read = message => ({
    type : "READ_MESSAGE" ,
    id : message._id
});

export const newNotification = notification => ({
    type : "NEW_NOTIFICATION",
    notification : notification.msg,
});

export const storeNotification = notification => ({
    type : "STORE_NOTIFICATION",
    notification : notification,
});

export const notificationActive = {
    type : "NOTIFICATION_ACTIVE"
};

