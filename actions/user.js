export const auth_token = token => ({
    type : 'STORE_TOKEN',
    token : token
});

export const user_info = info =>({
    type : 'STORE_USER_INFO',
    email : info.email,
})

export const set_user_name = name => ({
    type : 'STORE_USER_NAME',
    name : name
});

export const remove_user = {
    type : 'REMOVE_USER',
}

