import React from 'react';
import { Provider,connect } from 'react-redux';
import {reduxifyNavigator,createReactNavigationReduxMiddleware,createNavigationReducer} from 'react-navigation-redux-helpers';  
import { createSwitchNavigator } from 'react-navigation';
import Tabs from './navigators/bottom_navigator';
import AuthStack from './navigators/authenticationStack';
import { createStore,combineReducers,applyMiddleware } from 'redux';
import {user_reducer} from './reducers/authenticate';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { setChat, notification } from './reducers/match';

const Nav = createSwitchNavigator({
  Logstack : AuthStack,
  Tab : Tabs
},{
  initialRouteName : 'Logstack'
});

const navReducer = createNavigationReducer(Nav);
const appReducer = combineReducers({
  nav: navReducer,
  user : user_reducer,
  chat : setChat,
  notification : notification,
});
const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);
const App1 = reduxifyNavigator(Nav, "root");
const mapStateToProps = (state) => ({
  state: state.nav,
});
const AppWithNavigationState = connect(mapStateToProps)(App1);
const store = createStore(appReducer,applyMiddleware(thunk,middleware));

export default class App extends React.Component{

  render(){
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }

}