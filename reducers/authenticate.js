export const user_reducer = (state = {} , action) => {
    switch (action.type){

        case 'STORE_TOKEN' : {
            let new_state = {};
            Object.assign(new_state,state);
            new_state['token'] = action.token;
            return new_state;
        }

        case 'STORE_USER_INFO' : {
            let new_state = {};
            Object.assign(new_state,state);
            new_state['email'] = action.email;
            return new_state;
        }

        case 'STORE_USER_NAME' : {
            let new_state = {};
            Object.assign(new_state,state);
            new_state['name'] = action.name;
            return new_state;
        }


        case 'REMOVE_USER' : {
            let new_state = {};
            Object.assign(new_state,state);
            new_state['token'] = '';
            new_state['email'] = '';
            return new_state;
        }


        default : return state;
    }
}