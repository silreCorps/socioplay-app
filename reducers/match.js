export const setChat = (state = {} , action) => {
    if(action.type === "SET_CHAT"){
        let obj = {};
        Object.assign(obj,state);
        obj.id = action.id;
        obj.sport = action.sport;
        return obj;
    }
    else return state;
}

export const notification = (state = {new : false} ,action ) => {
    
    if(action.type === "NEW_NOTIFICATION"){
        let obj = {};
        Object.assign(obj,state);
        if(obj.notification){
            obj.notification.push({msg : action.notification});
        }
        else{
            obj.notification = [];
            obj.notification.push({msg : action.notification});
        }
        obj.new = true;
        return obj;
    }

    else if(action.type === "STORE_NOTIFICATION"){
        let obj = {};
        Object.assign(obj,state);
        obj.notification = action.notification;
        return obj;
    }

    else if(action.type === "NOTIFICATION_ACTIVE"){
        let obj = {};
        Object.assign(obj,state);
        obj.new = false;
        return obj;
    }
    else return state;
}