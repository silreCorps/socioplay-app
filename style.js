import { StyleSheet,Dimensions } from 'react-native';

export const style = StyleSheet.create({
    header_text:{
        alignSelf : 'flex-start',
        textAlign : 'center',
        width : Dimensions.get('window').width,
        fontSize : 18,
        color : "#0098fd",
        height : 30,
    },
    container: {
        marginTop : 26,
        height : Dimensions.get('window').height,  
        flex: 1,
        backgroundColor: '#fff',
    },
    header : {
        shadowColor : '#A6A4A4',
        shadowRadius : 10,
        shadowOpacity : 1,
        shadowOffset : {
            width : 0,
            height : 10
        },
        elevation : 5,
        backgroundColor : '#FAF9F9',
    }
})